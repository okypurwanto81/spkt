@extends('layouts.masterAdmin')
@section('titleAdmin')
@section('contentAdmin')
<div class="section-body">
    <div class="card-body">
        @if (session('message'))
                    <div class="alert alert-warning alert-dismissible show fade">
                        <div class="alert-body">
                          <button class="close" data-dismiss="alert">
                            <span>×</span>
                          </button>
                          {{ session('message') }}
                        </div>
                      </div>
                 </div>
                @endif
                <div class="row">
                    <div class="col-12">
                      <div class="card">
                        <div class="card-header">
                          <h4>Tabel STTP</h4>
                        </div>
                        <div class="card-body">
                          <div class="table-responsive">
                            <table class="table table-striped" id="table-1">
                              <thead>
                                <tr>
                                  <th class="text-center">
                                    No
                                  </th>
                                  <th>Nama</th>
                                  <th class="text-center">Status</th>
                                  <th>No hp</th>
                                  <th>Alamat</th>
                                  <th>Agama</th>
                                  <th class="text-center">tanggal dan waktu pembuatan</th>
                                  <th class="text-center">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                    $no = 1;
                                    foreach ($sttp_tabel as $data) : ?>
                                    <tr>
                                        <td class="text-center"><?= $no++; ?></td>
                                        <td><?= $data->nama; ?></td>
                                        <td class="text-center"><span class="badge <?=($data->status == 1) ? 'badge-success' : 'badge-danger' ; ?> badge-warning"><?=($data->status == 1) ? 'selesai' : 'Belum selesai' ; ?></span></td>
                                        <td><?= $data->nomer_hp; ?></td>
                                        <td><?= $data->alamat; ?></td>
                                        <td><?= $data->agama; ?></td>
                                        <td class="text-center"><?= $data->created_at; ?></td>
                                        <td>
                                            <div class="dropdown">
                                                <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                  Dropdown button
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                  <a href="{{ route('tabel.edit',$data->id) }}" class="dropdown-item" href="#">Edit</a>
                                                  <a href="#" data-id="{{ $data->id }}" class="dropdown-item swal-comfirm">
                                                    <form action="{{ route('tabel.delete',$data->id) }}" id="delete{{ $data->id }}" method="POST">
                                                       @method('delete')
                                                        @csrf
                                                    </form>

                                                    delete
                                                </a>
                                                <a href="{{ route('detai-data',$data->id) }}" class="dropdown-item">detail</a>
                                                <a href="{{ route('konfir', $data->id) }}" class="dropdown-item swal-2">Konfirmasi</a>
                                                </div>
                                              </div>
                                        </td>
                                        {{-- <td class="text-center">
                                            <a href="{{ route('tabel.edit',$data->id) }}" class="badge badge-primary">Edit</a>
                                             <a href="#" data-id="{{ $data->id }}" class="badge badge-dark swal-comfirm">
                                                <form action="{{ route('tabel.delete',$data->id) }}" id="delete{{ $data->id }}" method="POST">
                                                   @method('delete')
                                                    @csrf
                                                </form>
                                                delete
                                            </a>
                                            <a href="{{ route('detai-data',$data->id) }}" class="badge badge-primary btnDetail" data-nama="<?= $data->nama; ?>" data-nomer_hp="<?= $data->nomer_hp; ?>" data-agama="<?= $data->agama; ?>" data-alamat="<?= $data->alamat; ?>" data-tempat_kegiatan="<?= $data->tempat_kegiatan; ?>" data-acara="<?= $data->acara; ?>" data-jumlah_peserta="<?= $data->jumlah_peserta; ?>" data-kegiatan="<?= $data->kegiatan; ?>" data-organisasi="<?= $data->organisasi; ?>" data-tebusan="<?= $data->tebusan; ?>" data-tanggal_kegiatan="<?= $data->tanggal_kegiatan; ?>" data-created_at="<?= $data->created_at; ?>"data-toggle="modal" data-target="#ModaldetailSttp">detail</a>
                                            <a href="{{ route('konfir', $data->id) }}" class="badge badge-primary">Konfirmasi</a>
                                        </td> --}}
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
               </div>
            </div>
@endsection


@push('page-scriptsAdmin')
<script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>

@endpush

@push('after-scriptsAdmin')
<script>
$(".swal-comfirm").click(function(e) {
    id = e.target.dataset.id;
    swal({
        title: 'Yakin data di hapus?',
        text: 'Data yang sudah di hapus tidak bisa di balikin',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            // swal('Poof! Your imaginary file has been deleted!', {
            // icon: 'success',
            // });
            $(`#delete${id}`).submit();
        } else {
            // swal('Your imaginary file is safe!');
        }
      });
  });


  </script>

<script>
$(".swal-2").click(function() {

	swal('konfirmasi sukses', 'TEKAN TOMBOL OK', 'success');
});

</script>

<script>


    $('.btnDetail').on('click', function() {
        $('#containerNama').val($(this).data("nama"))
        $('#containerNomerhp').val($(this).data("nomer_hp"))
        $('#containerAgama').val($(this).data("agama"))
        $('#containerAlamat').val($(this).data("alamat"))
        $('#containerTempatkegiatan').val($(this).data("tempat_kegiatan"))
        $('#containerAcara').val($(this).data("acara"))
        $('#containerKegiatan').val($(this).data("kegiatan"))
        $('#containerOrganisasi').val($(this).data("organisasi"))
        $('#containerTebusan').val($(this).data("tebusan"))
        $('#containerTanggalkegiatan').val($(this).data("tanggal_kegiatan"))
        $('#containerjumlahpeserta').val($(this).data("jumlah_peserta"))
        $('#containercreated_at').val($(this).data("created_at"))
     })
  </script>
@endpush
