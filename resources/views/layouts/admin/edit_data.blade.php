@extends('layouts.masterAdmin')
@section('titleAdmin')
@section('contentAdmin')
<div class="section-body">
     <div class="row">
        <div class="col-12 col-md-10 col-lg-10">
          <div class="card">
            <div class="card-body">
                <h1 class="m-1">From Sttp :</h1>
         <form action="{{ route('tabel.update',$sttp_tabel->id) }}" method="POST">
            @csrf
            @method('patch')
            <div class="form-group">
                <label for="nama">1.Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{ $sttp_tabel->nama }}" placeholder="Nama">
              </div>
              <div class="form-group">
                <label>2.Tempat/Tanggal lahir</label>
                <input type="text" class="form-control" id="tempat_tanggal_lahir" name="tempat_tanggal_lahir" value="{{$sttp_tabel->tempat_tanggal_lahir }}" placeholder="Place/Date of Birth">
              </div>
              <div class="form-group">
                <label>3.Nomer hp</label>
                <input type="text" class="form-control" id="nomer_hp" name="nomer_hp" value="{{ $sttp_tabel->nomer_hp }}" placeholder="Place/Date of Birth">
              </div>
              <div class="form-group">
                <label>4.Agama</label>
                <input type="text" class="form-control" id="agama" name="agama" value="{{ $sttp_tabel->agama }}" placeholder="Religion">
              </div>
              <div class="form-group">
                <label>5.Alamat sekarang</label>
                <input type="text" class="form-control" id="alamat" name="alamat" value="{{ $sttp_tabel->alamat }}" placeholder="Current address">
              </div>
              <div class="form-group">
                <label>6.Kegiatan</label>
                <input type="text" class="form-control" id="kegiatan" name="kegiatan" value="{{ $sttp_tabel->kegiatan }}" placeholder="Citizen card number">
              </div>
              <div class="form-group">
                <label>7.Tempat Kegiatan</label>
                <input type="text" class="form-control" id="tempat_kegiatan" name="tempat_kegiatan" value="{{ $sttp_tabel->tempat_kegiatan }}" placeholder="Citizen card number">
              </div>
              <div class="form-group">
                <label>8.Tanggal kegiatan</label>
                <input type="text" class="form-control" id="tanggal_kegiatan" name="tanggal_kegiatan" value="{{ $sttp_tabel->tanggal_kegiatan }}" placeholder="Citizen card number">
              </div>
              <div class="form-group">
                <label>9.rangka</label>
                <input type="text" class="form-control" id="acara" name="acara" value="{{ $sttp_tabel->acara }}" placeholder="Citizen card number">
              </div>
              <div class="form-group">
                <label>10.Jumlah peserta</label>
                <input type="text" class="form-control" id="jumlah_peserta" name="jumlah_peserta" value="{{ $sttp_tabel->jumlah_peserta }}" placeholder="Citizen card number">
              </div>
              <div class="form-group">
                <label>11.tebusan</label>
                <input type="text" class="form-control" id="tebusan" name="tebusan"value="{{ $sttp_tabel->tebusan }}" placeholder="Citizen card number">
              </div>
              <div class="form-group">
                <label>12.organisasi</label>
                <input type="text" class="form-control" id="organisasi" name="organisasi"value="{{ $sttp_tabel->organisasi }}" placeholder="Citizen card number">
              </div>
              <div class="form-group">
              <button type="submit" class="btn btn-danger">Daftar</button>
              </div>
        </form>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection

@push('page-scriptsAdmin')

@endpush
