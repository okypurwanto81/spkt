@extends('layouts.masterAdmin')
@section('titleAdmin')
@section('contentAdmin')
<div class="section-body">
    <div class="col-12 mb-4">
        <div class="hero text-white hero-bg-image hero-bg-parallax" style="background-image: url('assets/img/unsplash/eberhard-grossgasteiger-1207565-unsplash.jpg');">
          <div class="hero-inner">
            <h2>Welcome, Admin!</h2>
            <p class="lead">You almost arrived, complete the information about your account to complete registration.</p>
          </div>
    </div>
  </div>
<label><h5>surat masuk</h5></label>
  <div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-primary">
          <i class="far fa-user"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>SKCK</h4>
          </div>
          <div class="card-body">
            {{ $skckBelumSelesai }}
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-danger">
          <i class="far fa-newspaper"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>STTP</h4>
          </div>
          <div class="card-body">
            {{ $sttpBelumSelesai }}
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-warning">
          <i class="far fa-file"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>SURAT IZIN KERAMAIAN</h4>
          </div>
          <div class="card-body">
           {{ $izinBelumSelesai }}
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-success">
          <i class="fas fa-circle"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>SURAT KEHILANGAN</h4>
          </div>
          <div class="card-body">
            {{ $kehilanganBelumSelesai }}
          </div>
        </div>
      </div>
    </div>
  </div>

  <label><h5>SUrat keluar</h5></label>
  <div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon bg-danger">
            <i class="far fa-newspaper"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>SKCK selesai</h4>
            </div>
            <div class="card-body">
              {{ $skckSelesai }}
            </div>
          </div>
        </div>
      </div>

    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon bg-danger">
            <i class="far fa-newspaper"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>STTP selesai</h4>
            </div>
            <div class="card-body">
              {{ $sttpSelesai }}
            </div>
          </div>
        </div>
      </div>


    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon bg-danger">
            <i class="far fa-newspaper"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>SURAT KEHILANGAN selesai</h4>
            </div>
            <div class="card-body">
                {{ $kehilanganSelesai }}
            </div>
          </div>
        </div>
      </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
          <div class="card-icon bg-danger">
            <i class="far fa-newspaper"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>SURAT IZIN selesai</h4>
            </div>
            <div class="card-body">
                {{ $izinSelesai }}
            </div>
          </div>
        </div>
      </div>
  </div>
</div>


@endsection

@push('page-scriptsAdmin')


@endpush
