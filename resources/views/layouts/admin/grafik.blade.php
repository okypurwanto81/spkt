@extends('layouts.masterAdmin')
@section('titleAdmin')
@section('contentAdmin')
<div class="section-body">
  <div class="row">
    <div class="col-12 col-md-6 col-lg-6">
      <div class="card">
        <div class="card-header">
          <h4>Line Chart</h4>
        </div>
        <div class="card-body">
          <canvas id="myChart"></canvas>
        </div>
      </div>
    </div>

    <div class="col-12 col-md-6 col-lg-6">
      <div class="card">
        <div class="card-header">
          <h4>Bar Chart</h4>
        </div>
        <div class="card-body">
          <canvas id="myChart2"></canvas>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection

@push('page-scriptsAdmin')


@endpush
