@extends('layouts.masterAdmin')
@section('titleAdmin')
@section('contentAdmin')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-10 col-lg-10">
          <div class="card">
            <div class="card-body">
                <h2 class="m-1">edit kehilangan :</h2>
            <form action="{{ route('kehilangan.update', $kehilangan_tabel->id) }}" method="POST" >
                @csrf
                @method('patch')
            <form>
            <div class="form-group">
                <label for="nama">1.Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="{{  $kehilangan_tabel->nama }}" placeholder="Nama">
              </div>

              <div class="form-group">
                <label>2.Tempat/Tanggal lahir</label>
                <input type="text" class="form-control" id="tempat_tanggal_lahir" name="tempat_tanggal_lahir" value="{{  $kehilangan_tabel->tempat_tanggal_lahir }}" placeholder="Place/Date of Birth">
              </div>

              <div class="form-group">
                <label>3.Nomer hp</label>
                <input type="text" class="form-control" id="nomer_hp" name="nomer_hp"  value="{{  $kehilangan_tabel->nomer_hp }}" placeholder="Place/Date of Birth">
              </div>

              <div class="form-group">
                <label>4.Agama</label>
                <input type="text" class="form-control" id="agama" name="agama" value="{{  $kehilangan_tabel->agama }}" placeholder="Religion">
              </div>

              <div class="form-group">
                <label>5.Alamat sekarang</label>
                <input type="text" class="form-control" id="alamat" name="alamat" value="{{  $kehilangan_tabel->alamat}}" placeholder="Current address">
              </div>

              <div class="form-group">
                <label>6.waktu kehilangan</label>
                <input type="text" class="form-control" id="waktu_kehilangan" name="waktu_kehilangan" value="{{  $kehilangan_tabel->waktu_kehilangan }}" placeholder="Citizen card number">
              </div>

              <div class="form-group">
                <label>8.tempat kehilangan</label>
                <input type="text" class="form-control" id="tempat_kehilangan" name="tempat_kehilangan" value="{{  $kehilangan_tabel->tempat_kehilangan}}" placeholder="Citizen card number">
              </div>

              <div class="form-group">
                <label>7.Tanggal kehilangan</label>
                <input type="date" class="form-control" id="tanggal_kehilanagn" name="tanggal_kehilanagn" value="{{  $kehilangan_tabel->tanggal_kehilanagn}}" placeholder="Citizen card number">
              </div>

              <div class="form-group">
                <label>9.keternagan</label>
                <input type="text" class="form-control" id="keterangan"  name="keterangan" value="{{  $kehilangan_tabel->keterangan }}" placeholder="Citizen card number">
              </div>

              <div class="form-group">
              <button type="submit" class="btn btn-danger">Daftar</button>
              </div>
        </form>
          </div>
          </div>
        </div>
        </div>
    </div>
@endsection

