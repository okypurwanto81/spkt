@extends('layouts.masterAdmin')
@section('titleAdmin')
@section('contentAdmin')
<div class="section-body">
    <div class="card-body">
        <label>
            <h2>Tabel STTP</h2>
        </label>
        @if (session('message'))
                    <div class="alert alert-warning alert-dismissible show fade">
                        <div class="alert-body">
                          <button class="close" data-dismiss="alert">
                            <span>×</span>
                          </button>
                          {{ session('message') }}
                        </div>
                      </div>
                @endif
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">gambar</th>
              <th scope="col">gambar</th>
              <th scope="col">gambar</th>
              <th scope="col">gambar</th>
              {{-- <th scope="col">Tempat/tanggal lahir</th> --}}
              {{-- <th scope="col">Action</th> --}}
            </tr>
          </thead>
          <tbody>
        @foreach ($upload_gambar_tabel as $no => $data )
                 <tr>
            <td>{{ $upload_gambar_tabel->firstitem()+$no }}</td>
            <td><a href="{{ asset('img/'. $data->input_fc_kk)}}"><img src="{{ asset('img/'. $data->input_fc_kk)}}" height="200%" width="90%" alt="" srcset=""></a></td>
            <td><a href="{{ asset('img/'. $data->input_fc_ktp)}}"><img src="{{ asset('img/'. $data->input_fc_ktp)}}" height="200%" width="90%" alt="" srcset=""></a></td>
            <td><a href="{{ asset('img/'. $data->input_fc_akte) }}"><img src="{{ asset('img/'. $data->input_fc_akte) }}" height="200%" width="50%" alt="" srcset=""></a></td>
            <td><a href="{{ asset('img/'. $data->input_fc_sidik_jari ) }}"><img src="{{ asset('img/'. $data->input_fc_sidik_jari ) }}" height="200%" width="50%" alt="" srcset=""></a></td>
            {{-- <td> --}}
                {{-- <a href="#" class="badge badge-primary">dwonload</a> --}}

                 {{-- <a href="#" data-id="{{ $data->id }}" class="badge badge-dark swal-comfirm">
                    <form action="{{ route('tabel.delete',$data->id) }}" id="delete{{ $data->id }}" method="POST">
                       @method('delete')
                        @csrf --}}
                        {{-- <button type="submit" data-id="{{ $data->id }}" class="btn btn-sm btn-primary swal-comfirm"> --}}
                    {{-- </form> --}}
                    {{-- delete
                </button> --}}
                    {{-- delete
                </a> --}}
                {{-- <a href="{{ route('detai-data',$data->id) }}" class="badge badge-primary">detail</a> --}}
            {{-- </td> --}}
            </tr>
        @endforeach
        </tbody>
        </table>
        {{ $upload_gambar_tabel->links() }}
    </div>
</div>


@endsection

@push('page-scriptsAdmin')
<script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>

@endpush

@push('after-scriptsAdmin')
<script>
$(".swal-comfirm").click(function(e) {
    id = e.target.dataset.id;
    swal({
        title: 'Yakin data di hapus?',
        text: 'Data yang sudah di hapus tidak bisa di balikin',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            // swal('Poof! Your imaginary file has been deleted!', {
            // icon: 'success',
            // });
            $(`#delete${id}`).submit();
        } else {
            // swal('Your imaginary file is safe!');
        }
      });
  });


  </script>
@endpush
