@extends('layouts.masterAdmin')
@section('titleAdmin')
@section('contentAdmin')
<div class="section-body">
    <div class="card-body">
        @if (session('message'))
        <div class="alert alert-warning alert-dismissible show fade">
            <div class="alert-body">
              <button class="close" data-dismiss="alert">
                <span>×</span>
              </button>
              {{ session('message') }}
            </div>
          </div>
    @endif
    </div>
    <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h4>Basic DataTables</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th class="text-center">
                        No
                      </th>
                      <th>Nama</th>
                      <th class="text-center">Status</th>
                      <th>No hp</th>
                      <th>Alamat</th>
                      <th>Agama</th>
                      <th class="text-center">tanggal dan waktu masuk surat</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                        $no = 1;
                        foreach ($kehilangan_tabel as $data) : ?>
                        <tr>
                            <td class="text-center"><?= $no++; ?></td>
                            <td><?= $data->nama; ?></td>
                            <td class="text-center"><span class="badge <?=($data->status == 1) ? 'badge-success' : 'badge-danger' ; ?> badge-warning"><?=($data->status == 1) ? 'selesai' : 'Belum selesai' ; ?></span></td>
                            <td><?= $data->nomer_hp; ?></td>
                            <td><?= $data->alamat; ?></td>
                            <td><?= $data->agama; ?></td>
                            <td class="text-center"><?= $data->created_at; ?></td>
                            <td>
                                <div class="dropdown">
                                    <button class="btn btn-outline-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Dropdown button
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="{{ route('edit.kehilangan',$data->id) }}" class="dropdown-item">Edit</a>
                                        <a href="#" data-id="{{ $data->id }}" class="dropdown-item swal-comfirm">
                                            <form action="{{ route('delete.kehilangan',$data->id) }}" id="delete{{ $data->id }}" method="POST">
                                             @method('delete')
                                            @csrf
                                            </form>
                                             delete
                                            </a>
                                            <a href="{{ route('hilang', $data->id) }}" class="dropdown-item">Konfirmasi</a>
                                    </div>
                                  </div>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@push('page-scriptsAdmin')
<script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>

@endpush

@push('after-scriptsAdmin')
<script>
$(".swal-comfirm").click(function(e) {
    id = e.target.dataset.id;
    swal({
        title: 'Yakin data di hapus?',
        text: 'Data yang sudah di hapus tidak bisa di balikin',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
            // swal('Poof! Your imaginary file has been deleted!', {
            // icon: 'success',
            // });
            $(`#delete${id}`).submit();
        } else {
            // swal('Your imaginary file is safe!');
        }
      });
  });


  </script>
@endpush
