<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <a href="{{ route('dasboard') }}">
        SPKT Tanjungpinang
        </a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="index.html">St</a>
      </div>
      <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>
        <li class="dropdown">
          <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
          <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ route('dasboard') }}">Dashboard</a></li>
            <li><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
          </ul>
        </li>
        <li class="menu-header">Form pendaftaran</li>
        <li class="dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-edit"></i> <span>Sttp</span></a>
          <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ route('sttp') }}">Form sttp</a></li>
            {{-- <li><a class="nav-link" href="#"></a></li>
            <li><a class="nav-link" href="#"></a></li> --}}
          </ul>
        </li>

        <li class="dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-edit"></i> <span>Surat izin keramaian</span></a>
          <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ route('izin') }}">Form surat izin keramaian</a></li>
            {{-- <li><a class="nav-link" href="#"></a></li>
            <li><a class="nav-link" href="#"></a></li> --}}
          </ul>
        </li>

        {{-- <li class="menu-header">Starter</li> --}}
        <li class="dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-edit"></i> <span>Surat kehilangan</span></a>
          <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ route('kehilangan') }}">Form surat kehilangan</a></li>
            {{-- <li><a class="nav-link" href="{{ route('izin') }}">Form surat izin keramaian</a></li>
            <li><a class="nav-link" href="layout-top-navigation.html">Top Navigation</a></li> --}}
          </ul>
        </li>

        {{-- <li class="menu-header">Starter</li> --}}
        <li class="dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-edit"></i> <span>Skck</span></a>
          <ul class="dropdown-menu">
            <li><a class="nav-link" href="{{ route('skck') }}">Form Skck</a></li>
            <li><a class="nav-link" href="{{ route('pertayaan') }}">DaftarPertayaan</a></li>
            {{-- <li><a class="nav-link" href="{{ route('izin') }}">Form surat izin keramaian</a></li>
            <li><a class="nav-link" href="layout-top-navigation.html">Top Navigation</a></li> --}}
          </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-edit"></i> <span>Upload persyaratan</span></a>
            <ul class="dropdown-menu">
              <li><a class="nav-link" href="{{ route('upload') }}">Form upload</a></li>
              {{-- <li><a class="nav-link" href="{{ route('izin') }}">Form surat izin keramaian</a></li>
              <li><a class="nav-link" href="layout-top-navigation.html">Top Navigation</a></li> --}}
            </ul>
          </li>
     </aside>
  </div>
