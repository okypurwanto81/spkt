@extends('layouts.master')
@section('title')
@section('content')
<div class="section-body">
        <div class="row">
        <div class="col-12 col-md-10 col-lg-10">
          <div class="card">
            <div class="card-body">
                <h1 class="m-1">From Sttp :</h1>
         <form action="{{ Route('simpan-data') }}" method="post">
            @csrf
            <div class="form-group">
                <label @error('nama')
                     class="text-danger"
                @enderror>1.Nama @error('nama')
                    |  {{ $message }}
                @enderror</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <i class="far fa-user"></i>
                      </div>
                    </div>
                <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}" placeholder="Nama">
              </div>
            </div>
              <div class="form-group">
                <label @error('tempat_tanggal_lahir')
                class="text-danger"
           @enderror>2.Tempat/Tanggal lahir @error('tempat_tanggal_lahir')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-calendar"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="tempat_tanggal_lahir" name="tempat_tanggal_lahir" value="{{ old('tempat_tanggal_lahir') }}" placeholder="Place/Date of Birth">
              </div>
              </div>
              <div class="form-group">
                <label @error('nomer_hp')
                class="text-danger"
           @enderror>3.Nomer hp @error('nomer_hp')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-phone"></i>
              </div>
            </div>
                <input type="number" class="form-control" id="nomer_hp" name="nomer_hp" value="{{ old('nomer_hp') }}" placeholder="Place/Date of Birth">
              </div>
              </div>
              <div class="form-group">
                <label @error('agama')
                class="text-danger"
           @enderror>4.Agama @error('agama')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-edit"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="agama" name="agama" value="{{ old('agama') }}" placeholder="Religion">
              </div>
              </div>
              <div class="form-group">
                <label @error('alamat')
                class="text-danger"
           @enderror>5.Alamat sekarang @error('alamat')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-map-marked-alt"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="alamat" name="alamat" value="{{ old('alamat') }}" placeholder="Current address">
              </div>
              </div>
              <div class="form-group">
                <label @error('kegiatan')
                class="text-danger"
           @enderror>6.Kegiatan @error('kegiatan')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-edit"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="kegiatan" name="kegiatan" value="{{ old('kegiatan') }}" placeholder="Citizen card number">
              </div>
              </div>
              <div class="form-group">
                <label @error('tempat_kegiatan')
                class="text-danger"
           @enderror>7.Tempat Kegiatan @error('tempat_kegiatan')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-edit"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="tempat_kegiatan" name="tempat_kegiatan" value="{{ old('tempat_kegiatan') }}" placeholder="Citizen card number">
              </div>
              </div>
              <div class="form-group">
                <label @error('tanggal_kegiatan')
                class="text-danger"
           @enderror>8.Tanggal kegiatan @error('tanggal_kegiatan')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-calendar"></i>
              </div>
            </div>
                <input type="date" class="form-control" id="tanggal_kegiatan" name="tanggal_kegiatan" value="{{ old('tanggal_kegiatan') }}" placeholder="Citizen card number">
              </div>
              </div>

              <div class="form-group">
                <label @error('tanggal_pembuatan')
                class="text-danger"
           @enderror>7.Tanggal pembuatan @error('tanggal_pembuatan')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-edit"></i>
              </div>
            </div>
                <input type="date" class="form-control" id="tanggal_pembuatan" name="tanggal_pembuatan" value="{{ old('tanggal_pembuatan') }}" placeholder="Citizen card number">
              </div>
              </div>

              <div class="form-group">
                <label @error('acara')
                class="text-danger"
           @enderror>9.rangka @error('acara')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-edit"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="acara" name="acara" value="{{ old('acara') }}" placeholder="Citizen card number">
              </div>
              </div>
              <div class="form-group">
                <label @error('jumlah_peserta')
                class="text-danger"
           @enderror>10.Jumlah peserta @error('jumlah_peserta')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-users"></i>
              </div>
            </div>
                <input type="number" class="form-control" id="jumlah_peserta" name="jumlah_peserta" value="{{ old('jumlah_peserta') }}" placeholder="Citizen card number">
              </div>
              </div>
              <div class="form-group">
                <label @error('tebusan')
                class="text-danger"
           @enderror>11.tebusan @error('tebusan')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-edit"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="tebusan" name="tebusan" value="{{ old('tebusan') }}" placeholder="Citizen card number">
              </div>
              </div>
              <div class="form-group">
                <label @error('organisasi')
                class="text-danger"
           @enderror>12.organisasi @error('organisasi')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-edit"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="organisasi" name="organisasi" value="{{ old('organisasi') }}" placeholder="Citizen card number">
              </div>
              </div>
              <input type="hidden" class="form-control" id="status" name="status" value="0">

              <div class="form-group">
              <button type="submit" class="btn btn-danger swal-2" >Daftar</button>
              </div>
        </form>
          </div>
          </div>
        </div>
        </div>

</div>



@endsection

@push('page-scripts')

@endpush

