@extends('layouts.master')
@section('title')
@section('content')
<div class="section-body">
    {{-- <div class="card">
        <div class="card-body"> --}}
        <div class="col-12 mb-4">
            <div class="hero text-white hero-bg-image hero-bg-parallax" style="background-image: url('assets/img/unsplash/andre-benz-1214056-unsplash.jpg');">
              <div class="hero-inner">
                <h2>Welcome</h2>
                <p class="lead">You almost arrived, complete the information about your account to complete registration.</p>
                {{-- <div class="mt-4">
                  <a href="#" class="btn btn-outline-white btn-lg btn-icon icon-left"><i class="far fa-user"></i> Setup Account</a>
                </div> --}}
              </div>
            </div>
          </div>
        {{-- </div>
    </div> --}}

    <div class="card-deck">
        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card">
          {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
          <div class="card-body">
            <h5 class="card-title">persayaratan</h5>
            <p class="card-text">
                <h5>SKCK</h5>
            </p>
            <a href="#" class="btn btn-primary">Lihat</a>
          </div>
        </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card">
          {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
          <div class="card-body">
            <h5 class="card-title">Persyaratan</h5>
            <p class="card-text">
               <h5>STTP</h5>
            </p>
            <a href="#" class="btn btn-primary">Lihat</a>
          </div>
        </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card">
          {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
          <div class="card-body">
            <h5 class="card-title">Persyaratan</h5>
            <p class="card-text">
                <h5>Surat Kehilangan</h5>
            </p>
            <a href="#" class="btn btn-primary">Lihat</a>
          </div>
        </div>
        </div>

        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card">
            {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
            <div class="card-body">
              <h5 class="card-title">Persayaratan</h5>
              <p class="card-text">
                  <h5>Surat izin keramaian</h5>
                </p>
                <a href="#" class="btn btn-primary" onclick="executeExample('mixin')">Lihat</a>
            </div>
          </div>
      </div>
    </div>
    <br>

    <div class="row">
        <div class="col-12 col-sm-6 col-lg-6">
    <div class="card">
        <div class="card-header">
          <h4>My Picture</h4>
        </div>
        <div class="card-body">
          <div class="mb-2 text-muted">Click the picture below to see the magic!</div>
          <div class="chocolat-parent">
            <a href="assets/img/example-image.jpg" class="chocolat-image" title="Just an example">
              <div data-crop-image="285">
                <img alt="image" src="assets/img/example-image.jpg" class="img-fluid">
              </div>
            </a>
          </div>
          <br>
          <div class="card-header-action">
            <a href="#" class="btn btn-primary">View All</a>
            <a href="#" class="btn btn-warning">View All</a>
          </div>
        </div>
      </div>
    </div>
</div>
</div>


@endsection

@if (session('message'))

@push('page-scripts')

<script src="{{ asset('assets/modules/sweetalert/sweetalert.min.js') }}"></script>
<script>
    $(document).ready(function() {
	swal('Pendaftaran Berhasil', 'You clicked the button!', 'success');
});
</script>

@endpush

@endif

<script>
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  Toast.fire({
    icon: 'success',
    title: 'Signed in successfully'
  })
</script>

