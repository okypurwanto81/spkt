@extends('layouts.master')
@section('title')
@section('content')
<div class="section-body">
    <script>
        var edu_row = 1;

        function addedu() {

            html = '<tr class="countEdu" id="edu_row' + edu_row + '">';
            html += '<td><input type="text" class="form-control" id="inputCity' + edu_row + '" name="riwayat_sekolah' + edu_row + '"><label for="inputState"></label></td>';
            html += '<td class="mt-10"><button class="badge badge-danger" onclick="removeEdu(' + edu_row + ')"><i class="fa fa-trash"></i> Delete</button></td>';

            html += '</tr>';

            $('#edu tbody').append(html);
            edu_row ++;
            var row = $('.countEdu').length;

            console.log(row);
            $('#jmlh_edu').val(row);
        }

        function removeEdu(id) {
            $('#edu_row' + id).remove();
            var row = $('.countEdu').length;
            $('#jmlh_edu').val(row);
            console.log(row);
        }
    </script>

<script>
    var year_row = 1;


    function addyear() {

        html = '<tr class="countyear" id="year_row' + year_row + '">';
        html += '<td><input type="text" class="form-control" id="grad' + year_row + '" name="tahun_lulus'+ year_row + '"><label for="inputState"></label></td>';
        html += '<td class="mt-10"><button class="badge badge-danger" onclick="removeYear(' + year_row + ')"><i class="fa fa-trash"></i> Delete</button></td>';

        html += '</tr>';

        $('#year tbody').append(html);

        year_row++;

        var row = $('.countyear').length;
        console.log(row);
        $('#jmlh_year').val(row);
    }

    function removeYear(id) {
        $('#year_row' + id).remove();
        var row = $('.countyear').length;
        $('#jmlh_year').val(row);
        console.log(row);

    }
</script>

    <form action="{{ route('pertayaan2.post') }}" method="POST">
        @csrf
        <div class="row">
            <div class="col-12 col-md-15 col-lg-15">
         <div class="card">
            <div class="card-body">
                <h6 class="m-1">l. Hubungan Kekeluargaan(Relationship):</h6>

            <div class="form-group">
                <p><label for="formGroupExampleInput">1.Istri/Suami(Wife/Husband):</label></p>
                <label for="formGroupExampleInput">a.Nama</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Name">
          </div>

          <div class="from-gruop">
                <label for="formGroupExampleInput">b.Umur</label>
                <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Age">
          </div>

          <div class="from-gruop">
                 <label for="formGroupExampleInput">c.Agama</label>
                 <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Religion">
          </div>

          <div class="from-gruop">
                <label for="formGroupExampleInput">d.Kebangsaan</label>
                 <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nationality">
          </div>

          <div class="from-gruop">
                <label for="formGroupExampleInput">e.Pekerjaan</label>
                 <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Occupation">
          </div>

          <div class="from-gruop">
                <label for="formGroupExampleInput">f.Alamat</label>
                 <input type="text" class="form-control" id="formGroupExampleInput" placeholder="address">
          </div>

          <p></p>
          <div class="from-gruop">
                 <p><label for="formGroupExampleInput">2.Bapak Sendiri (Father):</label></p>
             <label for="formGroupExampleInput">a.Nama</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Name">
          </div>

          <div class="from-gruop">
                 <label for="formGroupExampleInput">b.Umur</label>
             <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Age">
          </div>

          <div class="from-gruop">
            <label for="formGroupExampleInput">c.Agama</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Religion">
          </div>

          <div class="from-gruop">
            <label for="formGroupExampleInput">d.Kebangsaan</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nationality">
          </div>

          <div class="from-gruop">
            <label for="formGroupExampleInput">e.Pekerjaan</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Occupation">
          </div>

          <div class="from-gruop">
            <label for="formGroupExampleInput">f.Alamat sekarang</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Current address">
          </div>

          <p></p>
          <div class="from-gruop">
            <p><label for="formGroupExampleInput">3.ibu Sendiri (Mother):</label></p>
            <label for="formGroupExampleInput">a.Nama</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Name">
          </div>

          <div class="from-gruop">
            <label for="formGroupExampleInput">b.Umur</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Age">
          </div>

          <div class="from-gruop">
            <label for="formGroupExampleInput">c.Agama</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Religion">
          </div>

          <div class="from-gruop">
            <label for="formGroupExampleInput">d.Kebangsaan</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nationality">
          </div>

          <div class="from-gruop">
            <label for="formGroupExampleInput">e.Pekerjaan</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Occupation">
          </div>

          <div class="from-gruop">
            <label for="formGroupExampleInput">f.Alamat</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Address">
          </div>

          <p></p>
          <div class="form-row">
            <div class="form-group col-md-3">
                <p><label for="inputState">4.saudara sekandung/Tiri(siblings):</label></p>
                <label for="inputState">Nama(Name)</label>
                <input type="text" class="form-control" id="inputCity" placeholder="Nama(Name)">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
              </div>

              <div class="form-group col-md-2">
                <p><label for="inputState"></label></p>
                <label for="inputState">Umur(Age)</label>
                <input type="text" class="form-control" id="inputCity">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
              </div>

              <div class="form-group col-md-3">
                <p><label for="inputState"></label></p>
                <label for="inputState">Pekerjaan(Job)</label>
                <input type="text" class="form-control" id="inputCity">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
              </div>

              <div class="form-group col-md-3">
                <p><label for="inputState"></label></p>
                <label for="inputState">Alamat(Address)</label>
                <input type="text" class="form-control" id="inputCity">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
                <label for="inputState"></label>
                <input type="text" class="form-control" id="inputCity">
              </div>
            </div>

            <div class="form-row">
                <div class="form-group col-md-6">
                    <table id="edu" class="table table-hover">
                        <thead>
                        </thead>
                        <tbody>
                            <tr class="countEdu">
                                <td>
                                    <label for="inputState">15.Riwayat Sekolah(Education History)</label>
                                    <input type="text" class="form-control" id="inputCity1" name="riwayat_sekolah">
                                    <br>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="jmlh_edu" value="1" id="jmlh_edu">
                    <div class="text-center">
						<a href="javascript:void(0);" onclick="addedu();" class="badge badge-success">
							<i class="fa fa-plus"></i> ADD NEW
						</a>
					</div>
                  </div>

                  <div class="form-group col-md-6">
                    <table id="year" class="table table-hover">
                        <thead>
                        </thead>
                        <tbody>
                            <tr class="countyear">
                                <td>
                                    <label for="inputState">Tahun lulus(Year of Graduation)</label>
                                    <input type="text" class="form-control" id="grad1" name="tahun_lulus">
                                    <br>
                                </td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="jmlh_year" value="1" id="jmlh_year">
                    <div class="text-center">
						<a href="javascript:void(0);" onclick="addyear();" class="badge badge-success">
							<i class="fa fa-plus"></i> ADD NEW
						</a>
					</div>
                  </div>
                <br>

                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                      <li class="page-item"><a class="page-link" href="{{ Route('pertayaan') }}">Previous</a></li>
                      <li class="page-item"><a class="page-link" href="{{ Route('pertayaan') }}">1</a></li>
                      <li class="page-item"><a class="page-link" href="{{ Route('pertayaan2') }}">2</a></li>
                      <li class="page-item"><a class="page-link" href="{{ Route('pertayaan3') }}">3</a></li>
                      <li class="page-item"><a class="page-link" href="{{ Route('pertayaan3') }}">Next</a></li>
                    </ul>
                  </nav>
                </div>
            </div>
            <button type="submit">save</button>
         </div>
      </form>
</div>


@endsection

@push('page-scripts')


@endpush
