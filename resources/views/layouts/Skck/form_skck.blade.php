@extends('layouts.master')
@section('title')
@section('content')
<div class="section-body">
    <script>
        var anak_row = 2;

        function addanak() {

            html = '<tr class="countAnak" id="anak_row' + anak_row + '">';
            html += '<td><input type="text" class="form-control" id="inputCity' + anak_row + '" name="anak' + anak_row + '"><label for="inputState"></label></td>';
            html += '<td><input type="text" class="form-control" id="inputCity' + anak_row + '" name="umur_anak' + anak_row + '"><label for="inputState"></label></td>';
            html += '<td class="mt-10"><button class="badge badge-danger" onclick="removeAnak(' + anak_row + ')"><i class="fa fa-trash"></i> Delete</button></td>';

            html += '</tr>';

            $('#anak tbody').append(html);
            anak_row++;
            var row = $('.countAnak').length;

            console.log(row);
            $('#jmlh_anak').val(row);
        }

        function removeAnak(id) {
            $('#anak_row' + id).remove();
            var row = $('.countAnak').length;
            $('#jmlh_anak').val(row);
            console.log(row);
        }
    </script>

<script>
    var riwayat_row = 2;

    function addriwayat() {

        html = '<tr class="countRiwayat" id="riwayat_row' + riwayat_row + '">';
        html += '<td><input type="text" class="form-control" id="inputCity' + riwayat_row + '" name="riwayat' + riwayat_row + '"><label for="inputState"></label></td>';
        html += '<td><input type="text" class="form-control" id="inputCity' + riwayat_row + '" name="tahun' + riwayat_row + '"><label for="inputState"></label></td>';
        html += '<td class="mt-10"><button class="badge badge-danger" onclick="removeRiwayat(' + riwayat_row + ')"><i class="fa fa-trash"></i> Delete</button></td>';

        html += '</tr>';

        $('#riwayat tbody').append(html);
        riwayat_row++;
        var row = $('.countRiwayat').length;

        console.log(row);
        $('#jmlh_riwayat').val(row);
    }

    function removeRiwayat(id) {
        $('#riwayat_row' + id).remove();
        var row = $('.countRiwayat').length;
        $('#jmlh_riwayat').val(row);
        console.log(row);
    }
</script>

    <div class="row">
        <div class="col-12 col-md-15 col-lg-15">
          <div class="card">
            <div class="card-body">
                <h2 class="m-1">From SKCK:</h2>
                <form action="{{ Route('simpan-skck') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputEmail4">1.Nama lengkap</label>
                                <input name="nama" type="text" class="form-control" id="nama" placeholder="A.Nama lengkap">
                            </div>
                            <div class="form-group">
                                <label for="inputEmail4"></label>
                                <input name="panggilan" type="text" class="form-control" id="panggilan" placeholder="B.alias">
                            </div>
                            <div class="form-group">
                                <label for="text">2.Kebangsaan</label>
                                <input name="no_ktp" type="text" class="form-control" id="no_ktp" placeholder="A.Tgl.No.KTP">
                                <label for="inputEmail4"></label>
                                <input name="no_pasport" type="text" class="form-control" id="no_pasport" placeholder="B.Tgl./No.Pasport">
                            </div>

                        </div>
                        <div class="col-md-5">
                            <div class="col-xs-12 col-md-7 text-center" style="margin-left:-30px;">
                                <img class="ml-4" id="preview-foto"
                                    src="assets/img/4x6.png"
                                    alt="preview image" style="max-height: 200px;">
                                <input button class="col-md-5" type="file" name="foto" id="foto">

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">3.Agama</label>
                        <input name="agama" type="text" class="form-control" id="agama" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="text">4.data</label>
                        <input name="tanggal_lahir" type="date" class="form-control" id="tanggal_lahir" placeholder="a.tgl.lahir">
                        <label for="inputEmail4"></label>
                        <input name="tempat_lahir" type="text" class="form-control" id="tempat_lahir" placeholder="b.Tempat lahir">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputAddress">4.Jenis kelamin</label>
                        <input name="jenis_kelamin" type="text" class="form-control" id="inputAddress" placeholder="nomer">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputAddress">4.Keperluan surat</label>
                        <input name="perlu" type="text" class="form-control" id="inputAddress" placeholder="nomer">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputAddress">4.Nomer Hp</label>
                        <input name="no_hp" type="text" class="form-control" id="inputAddress" placeholder="nomer">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">5.Alamat</label>
                            <input name="alamat" type="text" class="form-control" id="alamat_tinggal" placeholder="Email">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">6.Perubahan Alamat</label>
                            <input name="alamat_baru" type="text" class="form-control" id="perubahan_alamt" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">7.Kedudukan dalam keluarga</label>
                            <input name="kedudukan" type="text" class="form-control" id="kedudukan_keluarga" placeholder="Email">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">8. a.Nama Bapak</label>
                            <input name="nama_bapak" type="text" class="form-control" id="nama_bapak" placeholder="a.Nama Bapak">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">a.Nama Ibu</label>
                            <input name="nama_ibu" type="text" class="form-control" id="nama_ibu" placeholder="Nama Ibu">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputPassword4">b.Alamat</label>
                            <input name="alamat_keluarga" type="text" class="form-control" id="alamat_bapak_ibu" placeholder="Alamat Orang tua">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">9. Pekerjaan</label>
                        <input name="pekerjaan" type="text" class="form-control" id="pekerjaan" placeholder="Pekerjaan">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputCity">10. a.Nama Suami/Istri</label>
                            <input name="suami_istri" type="text" class="form-control" id="nama_suami_istri">

                            <label for="inputCity">b.Nama Bapak Suami/Istri</label>
                            <input name="bapak_suami" type="text" class="form-control" id="nama_bapak_suami">

                            <label for="inputCity">c.Nama Ibu Suami/Istri</label>
                            <input name="ibu_istri" type="text" class="form-control" id="nama_ibu_istri">

                            <label for="inputCity">d.Alamat</label>
                            <input name="alamat_suami_istri" type="text" class="form-control" id="alamat_suami_istri">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputState">Umur</label>
                            <input name="umur_suami_istri" type="text" class="form-control" id="umur_suami_istri">
                            <label for="inputState">umur</label>
                            <input name="umur_bapak_suami" type="text" class="form-control" id="umur_bapak_suami">
                            <label for="inputState">umur</label>
                            <input name="umur_ibu_istri" type="text" class="form-control" id="umur_ibu_istri">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputZip">11. Sanak/saudara yang menjadi tanggungan</label>
                            <input name="saudara_tanggung" type="text" class="form-control" id="Alamat_saudara">
                            <label for="inputZip">Alamat</label>
                            <input name="alamat_saudara" type="text" class="form-control" id="Alamat_saudara">
                        </div>

                        <div class="form-group col-md-12">
                            <table id="anak" class="table table-hover">
                                <thead>
                                </thead>
                                <tbody>
                                    <tr class="countAnak">
                                        <td>
                                            <label for="inputState">12.anak</label>
                                            <input type="text" class="form-control" id="inputCity1" name="anak1">
                                            <br>
                                        </td>
                                        <td>
                                            <label for="inputState">Umur</label>
                                            <input name="umur_anak1" type="text" class="form-control" id="inputCity1">
                                            <br>
                                        </td>

                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type="hidden" name="jmlh_anak" value="1" id="jmlh_anak">
                            <div class="text-center">
                                <a href="javascript:void(0);" onclick="addanak();" class="badge badge-success">
                                    <i class="fa fa-plus"></i> ADD NEW
                                </a>
                            </div>
                          </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputState">13.Ciri-ciri badan</label>
                            <input name="ciri_fisik1" type="text" class="form-control" id="ciri_fisik">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputState"></label>
                            <input name="ciri_fisik2" type="text" class="form-control" id="ciri_fisik">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputState"></label>
                            <input name="ciri_fisik3" type="text" class="form-control" id="ciri_fisik">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputState"></label>
                            <input name="ciri_fisik4" type="text" class="form-control" id="ciri_fisik">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputState"></label>
                            <input name="ciri_fisik5" type="text" class="form-control" id="ciri_fisik">
                        </div>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="inputAddress">14.Rumusan Sidik Jari</label>
                        <input name="rumus_sidik_jari" type="text" class="form-control" id="inputAddress" placeholder="Pekerjaan">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <table id="riwayat" class="table table-hover">
                                <thead>
                                </thead>
                                <tbody>
                                    <tr class="countRiwayat">
                                        <td>
                                            <label for="inputState">15.Riwayat sekolah</label>
                                            <input type="text" class="form-control" id="inputCity1" name="riwayat1">
                                            <br>
                                        </td>
                                        <td>
                                            <label for="inputState">Tahun lulus</label>
                                            <input type="text" class="form-control" id="inputCity1" name="tahun1">
                                            <br>
                                        </td>

                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                            <input type="hidden" name="jmlh_riwayat" value="1" id="jmlh_riwayat">
                            <div class="text-center">
                                <a href="javascript:void(0);" onclick="addriwayat();" class="badge badge-success">
                                    <i class="fa fa-plus"></i> ADD NEW
                                </a>
                            </div>
                          </div>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="inputAddress">16.Kesenangan/Kegemaran/Hobi</label>
                        <input name="hobi" type="text" class="form-control" id="hobi" placeholder="Hobi">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="inputState">17.Catatan Keriminal yang ada</label>
                            <input name="catatan_kriminal1" type="text" class="form-control" id="catatan_kriminal">
                            <label for="inputState"></label>
                            <input name="catatan_kriminal2" type="text" class="form-control" id="catatan_kriminal">
                            <label for="inputState"></label>
                            <input name="catatan_kriminal3" type="text" class="form-control" id="catatan_kriminal">
                            <label for="inputState"></label>
                            <input name="catatan_kriminal4" type="text" class="form-control" id="catatan_kriminal">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputState">18.Data/Keterangan-2 lain</label>
                            <input name="data_keterangan1" type="text" class="form-control" id="data_keterangan">
                            <label for="inputState"></label>
                            <input name="data_keterangan2" type="text" class="form-control" id="data_keterangan">
                            <label for="inputState"></label>
                            <input name="data_keterangan3" type="text" class="form-control" id="data_keterangan">
                            <label for="inputState"></label>
                            <input name="data_keterangan4" type="text" class="form-control" id="data_keterangan">
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-danger">Daftar</button>
                        </div>
                </form>
            </div>
          </div>
        </div>
    </div>
</div>


@endsection

@push('page-scripts')


@endpush
