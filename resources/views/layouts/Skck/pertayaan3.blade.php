@extends('layouts.master')
@section('title')
@section('content')
<div class="section-body">
    <form>
        <div class="col-12 col-md-15 col-lg-15">
            <div class="card">
               <div class="card-body">
            <div class="form-group">
                <p><label for="formGroupExampleInput">1.Perkara Pidana (Criminal Case):</label></p>
            <label for="formGroupExampleInput">a.Apakah Saudara pernah tersangkut Perkara pidana?/Have you ever caught in a criminal case?</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Jawab/answer">
          </div>
          <div class="from-gruop">
            <label for="formGroupExampleInput">b.Dalam perkara apa? / In wahat kind of case?</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Jawab/answer">
          </div>
          <div class="from-gruop">
            <label for="formGroupExampleInput">c.Bagaimana putusannya dan vonis hakim?/ What is the descision of the judge and verdict?</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Jawab/answer">
          </div>
          <div class="from-gruop">
            <label for="formGroupExampleInput">d.Apakah Saudara sedang dalam proses perkara pidanan?Kasus apa?/Are you currently in the process of a criminal case? wahat kind of case?</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Jawab/answer">
          </div>
          <div class="from-gruop">
            <label for="formGroupExampleInput">e.Sampai sejauh mana proses hukumnya?/To what extent is the legal process?</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Jawab/answer">
          </div>
          <p></p>
          <div class="from-gruop">
            <p><label for="formGroupExampleInput">2. Pelanggaran (Violation):</label></p>
            <label for="formGroupExampleInput">a.Apakah Saudara pernah melakukan pelanggaran hukum dan atau norma-norma sosial?/ Have you ever violated the law and social norms or other? </label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Jawab/answer">
          </div>
          <div class="from-gruop">
            <label for="formGroupExampleInput">b.Pelanggaran hukum atau norma-norma sosial apa?/ What kind of violations of the law?</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Jawab/answer">
          </div>
          <div class="from-gruop">
            <label for="formGroupExampleInput">c.sampai sejauh mana prosesnya?/ To what extent is the process?</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Jawab/answer">
          </div>
          <p></p>
          <div class="from-gruop">
            <p><label for="formGroupExampleInput">lll. INFORMASI LAIN (OTHER INFORMATION)</label></p>
            <label for="formGroupExampleInput">1.Riwayat pekerjaan / negara-negar yang pernah dikunjungi/work exprience and country you have ever visited before:(sebutkan tahun berapa,dalam rangka apa dan negara mana yang dikunjungi)</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Name">
          </div>
          <div class="from-gruop">
            <label for="formGroupExampleInput">2.Kesenangan/ kegemaran/ hobi/ pleasure/ Fondness/ hobbies:</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Age">
          </div>
          <div class="from-gruop">
            <label for="formGroupExampleInput">3.Alamat yang mudah dihubungi(no.telpon)/Contact no. in case of emergency (phone number)</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Religion">
          </div>
          <p></p>
          <div class="from-gruop">
            <p><label for="formGroupExampleInput">lV.Sponsor (khusus orang asing/ foreigners)</label></p>
            <label for="formGroupExampleInput">1.Disponsori oleh(sponsored by)</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Name">
          </div>
          <div class="from-gruop">
            <label for="formGroupExampleInput">2.Alamat seponsor (sponsor Address)</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Age">
          </div>
          <div class="from-gruop">
            <label for="formGroupExampleInput">3.Telp./Fax.(Tel./Fax.)</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Religion">
          </div>
          <div class="from-gruop">
            <label for="formGroupExampleInput">4.Jenis usaha(Type of Bussiness)</label>
            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Nationality">
          </div>
          <br>
          <nav aria-label="Page navigation example">
            <ul class="pagination">
              <li class="page-item"><a class="page-link" href="{{ Route('pertayaan2') }}">Previous</a></li>
              <li class="page-item"><a class="page-link" href="{{ Route('pertayaan') }}">1</a></li>
              <li class="page-item"><a class="page-link" href="{{ Route('pertayaan2') }}">2</a></li>
              <li class="page-item"><a class="page-link" href="{{ Route('pertayaan3') }}">3</a></li>
              {{-- <li class="page-item"><a class="page-link" href="#">Next</a></li> --}}
            </ul>
          </nav>
        <button type="submit" class="btn btn-primary">send</button>
        </div>
            </div>
        </div>
      </form>
</div>


@endsection

@push('page-scripts')


@endpush


