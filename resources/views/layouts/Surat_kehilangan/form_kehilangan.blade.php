@extends('layouts.master')
@section('title')
@section('content')
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-10 col-lg-10">
          <div class="card">
            <div class="card-body">
                <h2 class="m-1">From surat kehilangan :</h2>
            <form action="{{ route('simpan-kehilangan') }}" method="POST">
                @csrf
            <div class="form-group">
                <label @error('nama')
                     class="text-danger"
                @enderror>1.Nama @error('nama')
                    |  {{ $message }}
                @enderror</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <i class="far fa-user"></i>
                      </div>
                    </div>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama">
              </div>
            </div>
              <div class="form-group">
                <label @error('tempat_tanggal_lahir')
                class="text-danger"
           @enderror>2.Tempat/Tanggal lahir @error('tempat_tanggal_lahir')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-calendar"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="tempat_tanggal_lahir" name="tempat_tanggal_lahir" placeholder="Place/Date of Birth">
              </div>
              </div>
              <div class="form-group">
                <label @error('nomer_hp')
                class="text-danger"
           @enderror>3.Nomer hp @error('nomer_hp')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-phone"></i>
              </div>
            </div>
                <input type="number" class="form-control" id="nomer_hp" name="nomer_hp" placeholder="Place/Date of Birth">
              </div>
              </div>
              <div class="form-group">
                <label @error('agama')
                class="text-danger"
           @enderror>4.Agama @error('agama')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-edit"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="agama" name="agama" placeholder="Religion">
              </div>
              </div>
              <div class="form-group">
                <label @error('alamat')
                class="text-danger"
           @enderror>5.Alamat @error('alamat')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-map-marked-alt"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Current address">
              </div>
              </div>
              <div class="form-group">
                <label @error('waktu_kehilangan')
                class="text-danger"
           @enderror>6.waktu kehilangan @error('waktu_kehilangan')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-clock"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="waktu_kehilangan" name="waktu_kehilangan" placeholder="Citizen card number">
              </div>
              </div>
              <div class="form-group">
                <label @error('tanggal_kehilanagn')
                class="text-danger"
           @enderror>7.Tanggal Kehilangan @error('tanggal_kehilanagn')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-calendar"></i>
              </div>
            </div>
                <input type="date" class="form-control" id="tanggal_kehilanagn" name="tanggal_kehilanagn" placeholder="Citizen card number">
              </div>
              </div>
              <div class="form-group">
                <label @error('tempat_kehilangan')
                class="text-danger"
           @enderror>8.tempat terakhir barang @error('tempat_kehilangan')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-map-marked-alt"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="tempat_kehilangan" name="tempat_kehilangan" placeholder="Citizen card number">
              </div>
              </div>
              <div class="form-group">
                <label @error('keterangan')
                class="text-danger"
           @enderror>9.keterangan @error('keterangan')
               |  {{ $message }}
           @enderror</label>
           <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <i class="fas fa-edit"></i>
              </div>
            </div>
                <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Citizen card number">
              </div>
              </div>
              <div class="form-group">
              <button type="submit" class="btn btn-danger">Daftar</button>
              </div>
        </form>
          </div>
          </div>
        </div>
        </div>
</div>


@endsection

@push('page-scripts')


@endpush
