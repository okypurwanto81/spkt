<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>@yield('titleAdmin') {{ config('app.name') }}</title>

  <!-- General CSS Files -->
  <link rel="stylesheet" href="{{asset('assets/modules/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/modules/fontawesome/css/all.min.css')}}">

  <!-- CSS Libraries -->
 <link rel="stylesheet" href="{{asset('assets/modules/datatables/datatables.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css')}}">


  <!-- Template CSS -->
  <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('assets/css/components.css')}}">

  @stack('page-stylesAdmin')

<!-- Start GA -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-94034622-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-94034622-3');
</script>
<!-- /END GA --></head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>

      @include('layouts.admin.headerAdmin')
      @include('layouts.admin.siderbarAdmin')


      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>SPKT Tanjungpinang</h1>
                <div class="section-header-breadcrumb">
                  <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                  <div class="breadcrumb-item">SPKT</div>
                </div>
              </div>
          @yield('contentAdmin')
        </section>
        {{-- <div class="modal fade" tabindex="-1" role="dialog" id="ModaldetailSttp">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Detail data</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <form>
                        <fieldset disabled>
                          <div class="form-group">
                            <label for="disabledTextInput">Disabled input</label>
                            <input type="text" id="Nama" class="form-control" placeholder="Disabled input">
                          </div>

                          <fieldset disabled>
                            <div class="form-group">
                              <label for="disabledTextInput">Disabled input</label>
                              <input type="text" id="containerNomerhp" class="form-control" placeholder="Disabled input">
                            </div>

                            <fieldset disabled>
                                <div class="form-group">
                                  <label for="disabledTextInput">Disabled input</label>
                                  <input type="text" id="containerAgama" class="form-control" placeholder="Disabled input">
                                </div>

                                <fieldset disabled>
                                    <div class="form-group">
                                      <label for="disabledTextInput">Disabled input</label>
                                      <input type="text" id="containerAlamat" class="form-control" placeholder="Disabled input">
                                    </div>

                                    <fieldset disabled>
                                        <div class="form-group">
                                          <label for="disabledTextInput">Disabled input</label>
                                          <input type="text" id="containerTempatkegiatan"  class="form-control" placeholder="Disabled input">
                                        </div>

                                        <fieldset disabled>
                                            <div class="form-group">
                                              <label for="disabledTextInput">Disabled input</label>
                                              <input type="text" id="containerAcara" class="form-control" placeholder="Disabled input">
                                            </div>

                                            <fieldset disabled>
                                                <div class="form-group">
                                                  <label for="disabledTextInput">Disabled input</label>
                                                  <input type="text" id="containerjumlahpeserta"  class="form-control" placeholder="Disabled input">
                                                </div>

                                                <fieldset disabled>
                                                    <div class="form-group">
                                                      <label for="disabledTextInput">Disabled input</label>
                                                      <input type="text" id="containerKegiatan"  class="form-control" placeholder="Disabled input">
                                                    </div>

                                                    <fieldset disabled>
                                                        <div class="form-group">
                                                          <label for="disabledTextInput">Disabled input</label>
                                                          <input type="text" id="containerOrganisasi"  class="form-control" placeholder="Disabled input">
                                                        </div>

                                                        <fieldset disabled>
                                                            <div class="form-group">
                                                              <label for="disabledTextInput">Disabled input</label>
                                                              <input type="text" id="containerTebusan"  class="form-control" placeholder="Disabled input">
                                                            </div>

                                                            <fieldset disabled>
                                                                <div class="form-group">
                                                                  <label for="disabledTextInput">Disabled input</label>
                                                                  <input type="text" id="containerTanggalkegiatan"  class="form-control" placeholder="Disabled input">
                                                                </div>

                                                                <fieldset disabled>
                                                                    <div class="form-group">
                                                                      <label for="disabledTextInput">Disabled input</label>
                                                                      <input type="text" id="containercreated_at"  class="form-control" placeholder="Disabled input">
                                                                    </div>

                           </form>
                </div>
                <div class="modal-footer bg-whitesmoke br">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">batal</button>
                  <button type="button" class="btn btn-primary">dwonload</button>
                </div>
              </div>
            </div>
          </div> --}}
      </div>
      </div>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2021
        </div>
        <div class="footer-right">

        </div>
      </footer>
    </div>
  </div>





  @stack('before-scriptsAdmin')
  <!-- General JS Scripts -->
  <script src="{{asset('assets/modules/jquery.min.js')}}"></script>
  <script src="{{asset('assets/modules/popper.js')}}"></script>
  <script src="{{asset('assets/modules/tooltip.js')}}"></script>
  <script src="{{asset('assets/modules/bootstrap/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
  <script src="{{asset('assets/modules/moment.min.js')}}"></script>
  <script src="{{asset('assets/js/stisla.js')}}"></script>

  <!-- JS Libraies -->
@stack('page-scriptsAdmin')
<script src="{{asset('assets/modules/datatables/datatables.min.js')}}"></script>
<script src="{{asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js')}}"></script>
<script src="{{asset('assets/modules/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/modules/chart.min.js')}}"></script>



  <!-- Page Specific JS File -->

@stack('after-scriptsAdmin')
<script src="{{asset('assets/js/page/modules-datatables.js')}}"></script>
<script src="{{asset('assets/js/page/modules-chartjs.js')}}"></script>



  <!-- Template JS File -->
  <script src="{{asset('assets/js/scripts.js')}}"></script>
  <script src="{{asset('assets/js/custom.js')}}"></script>

  <script type="text/javascript">
    $(document).ready(function (e) {


        $('#foto').change(function () {

            let reader = new FileReader();

            reader.onload = (e) => {

                $('#preview-foto').attr('src', e.target.result);
            }

            reader.readAsDataURL(this.files[0]);

        });

    });

</script>


</body>
</html>
