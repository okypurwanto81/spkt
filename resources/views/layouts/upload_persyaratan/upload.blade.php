@extends('layouts.master')
@section('title')
@section('content')
<div class="section-body">
    <div class="card">
        <div class="card-body">
            @if (session('message'))
            <div class="alert alert-warning alert-dismissible show fade">
                <div class="alert-body">
                  <button class="close" data-dismiss="alert">
                    <span>×</span>
                  </button>
                  {{ session('message') }}
                </div>
              </div>
        @endif
            {{-- <form> --}}
        <form method="POST" action="{{ route('simpan-gambar') }}" enctype="multipart/form-data">
            @csrf
            <label>1.scan foto copy kk</label>
            <div class="col-md-5">
                <div class="col-xs-12 col-md-7 text-center" style="margin-left:-30px;">
                    <img class="ml-4" id="preview-fc_kk"
                        src="assets/img/4x6.png"
                        alt="preview image" style="max-height: 200px;">
                    <input button class="col-md-5" type="file" name="input_fc_kk" id="input_fc_kk">

                </div>
            </div>

         <label>2.scan ktp</label>
         <div class="col-md-5">
            <div class="col-xs-12 col-md-7 text-center" style="margin-left:-30px;">
                <img class="ml-4" id="preview-fc_ktp"
                    src="assets/img/4x6.png"
                    alt="preview image" style="max-height: 200px;">
                <input button class="col-md-5" type="file" name="input_fc_ktp" id="input_fc_ktp">

            </div>
        </div>

        <br>
        <label>3.scan akte kelahiran</label>
        <div class="col-md-5">
        <div class="col-xs-12 col-md-7 text-center" style="margin-left:-30px;">
            <img class="ml-4" id="preview-fc_akte"
                src="assets/img/4x6.png"
                alt="preview image" style="max-height: 200px;">
            <input button class="col-md-5" type="file" name="input_fc_akte" id="input_fc_akte">

        </div>
        </div>
        <br>
        <label>4.scan bukti sidik jari</label>
        <div class="col-md-5">
        <div class="col-xs-12 col-md-7 text-center" style="margin-left:-30px;">
            <img class="ml-4" id="preview-fc_sidik_jari"
                src="assets/img/4x6.png"
                alt="preview image" style="max-height: 200px;">
            <input button class="col-md-5" type="file" name="input_fc_sidik_jari" id="input_fc_sidik_jari">

        </div>
        </div>

        <div class="form-group">
        <button type="submit" class="btn btn-primary">send</button>
        </div>
        </form>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')


@endpush

