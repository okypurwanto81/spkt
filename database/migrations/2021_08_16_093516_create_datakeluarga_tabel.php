<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatakeluargaTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datakeluarga_tabel', function (Blueprint $table) {
            $table->id();
            $table->string('suami_istri');
            $table->string('bapak_suami');
            $table->string('ibu_istri');
            $table->text('alamat_suami_istri');
            $table->string('umur_suami_istri');
            $table->string('umur_bapak_suami');
            $table->string('umur_ibu_istri');
            $table->string('saudara_tanggung');
            $table->text('alamat_saudara');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datakeluarga_tabel');
    }
}
