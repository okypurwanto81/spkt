<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIzinTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('izin_tabel', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('tempat_tanggal_lahir');
            $table->string('agama');
            $table->string('alamat');
            $table->string('nomer_hp');
            $table->string('tempat_kegiatan');
            $table->string('acara');
            $table->date('tanggal_kegiatan');
            $table->string('jumlah_peserta');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('izin_tabel');
    }
}
