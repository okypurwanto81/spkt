<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertayaanTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertayaan_tabel', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('tanggal');
            $table->string('agama');
            $table->string('kebangsaan');
            $table->string('jenis_kelamin');
            $table->string('status_a');
            $table->string('kerja');
            $table->string('alamat');
            $table->string('ktp');
            $table->string('no_paspor');
            $table->string('no_kitas_kitap');
            $table->string('no_hp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertayaan_tabel');
    }
}
