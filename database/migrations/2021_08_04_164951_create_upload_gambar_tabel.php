<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadGambarTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_gambar_tabel', function (Blueprint $table) {
            $table->id();
            $table->string('input_fc_kk');
            $table->string('input_fc_ktp');
            $table->string('input_fc_akte');
            $table->string('input_fc_sidik_jari');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_gambar_tabel');
    }
}
