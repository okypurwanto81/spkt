<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkckTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skck_tabel', function (Blueprint $table) {
            $table->id();
            $table->string('ciri_fisik1');
            $table->string('ciri_fisik2');
            $table->string('ciri_fisik3');
            $table->string('ciri_fisik4');
            $table->string('ciri_fisik5');
            $table->string('rumus_sidik_jari');
            $table->string('riwayat_sekolah1');
            $table->string('riwayat_sekolah2');
            $table->string('riwayat_sekolah3');
            $table->string('riwayat_sekolah4');
            $table->string('riwayat_sekolah5');
            $table->string('riwayat_sekolah6');
            $table->string('tahun_lulus1');
            $table->string('tahun_lulus2');
            $table->string('tahun_lulus3');
            $table->string('tahun_lulus4');
            $table->string('tahun_lulus5');
            $table->string('tahun_lulus6');
            $table->string('hobi');
            $table->string('catatan_kriminal1');
            $table->string('catatan_kriminal2');
            $table->string('catatan_kriminal3');
            $table->string('catatan_kriminal4');
            $table->string('data_keterangan1');
            $table->string('data_keterangan2');
            $table->string('data_keterangan3');
            $table->string('data_keterangan4');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_skck_tabel');
    }
}
