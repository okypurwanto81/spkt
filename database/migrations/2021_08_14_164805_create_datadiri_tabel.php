<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatadiriTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datadiri_tabel', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('panggilan');
            $table->string('no_ktp');
            $table->string('no_pasport');
            $table->string('agama');
            $table->string('jenis_kelamin');
            $table->date('tanggal_lahir');
            $table->string('tempat_lahir');
            $table->text('alamat');
            $table->string('alamat_baru');
            $table->string('kedudukan');
            $table->string('nama_bapak');
            $table->string('nama_ibu');
            $table->text('alamat_keluarga');
            $table->string('pekerjaan');
            $table->string('foto');
            $table->string('no_hp');
            $table->string('perlu');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datadiri_tabel');
    }
}
