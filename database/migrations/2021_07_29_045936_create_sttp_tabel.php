<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSttpTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sttp_tabel', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('tempat_tanggal_lahir');
            $table->string('agama');
            $table->string('alamat');
            $table->string('nomer_hp');
            $table->string('kegiatan');
            $table->string('tempat_kegiatan');
            $table->string('acara');
            $table->date('tanggal_kegiatan');
            $table->string('jumlah_peserta');
            $table->string('tebusan');
            $table->string('organisasi');
            $table->string('status');
            $table->date('tanggal_pembuatan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_sttp_tabel');
    }
}
