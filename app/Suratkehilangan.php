<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suratkehilangan extends Model
{
    protected $guarded = [];
    protected $table = 'kehilangan_tabel';
}
