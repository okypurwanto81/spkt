<?php

namespace App\Http\Controllers;

use App\antrian;
use App\Sttpindex;
use Illuminate\Http\Request;

class Sttp2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.Sttp.form_sttp');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nama'          => 'required|max:20|min:3',
            'tempat_tanggal_lahir'   => 'required|max:100|min:3',
            'agama'          => 'required|max:100|min:3 ',
            'alamat'          => 'required|max:100|min:3',
            'nomer_hp'          => 'required|max:100|min:1',
            'tempat_kegiatan'    => 'required|max:100|min:3',
            'acara'          => 'required|max:100|min:3',
            'jumlah_peserta'          => 'required|max:100|min:1',
            'kegiatan'      => 'required|max:100|min:3',
            'organisasi'   => 'required|max:100|min:3',
            'tebusan'   => 'required|max:100|min:3',
            'tanggal_kegiatan' => 'required|max:100|min:3',
            'status' => 'required',
            'tanggal_pembuatan' => 'required|max:100|min:3',

        ],
    [
        'nama.required'  => 'Form harus di isi sesuai KTP',
        'tempat_tanggal_lahir.required'  => 'Form harus di isi',
        'agama.required'  => 'Form harus di isi',
        'alamat.required'  => 'Form harus di isi',
        'nomer_hp.required'  => 'Form harus di isi',
        'tempat_kegiatan.required'  => 'Form harus di isi',
        'acara.required'  => 'Form harus di isi',
        'jumlah_peserta.required'  => 'Form harus di isi',
        'kegiatan.required'  => 'Form harus di isi',
        'organisasi.required'  => 'Form harus di isi',
        'tebusan.required'  => 'Form harus di isi',
        'tanggal_kegiatan.required'  => 'Form harus di isi',
        'tanggal_pembuatan.required'  => 'Form harus di isi',


    ]

);
    $insertdata = Sttpindex::create($request->all());

    if ($insertdata){
        $nomor_antrian_terakhir=$this->getAntrian($request->tanggal_pembuatan);

        antrian::create([
            'nomor_antrian'=> $nomor_antrian_terakhir,
            'pendaftar_id'=> $insertdata->id,
            'tanggal'=> $request->tanggal_pembuatan,
            'is_finished'=>0,
        ]);
    }


    return Redirect('index')->with('message', 'Data berhasil di dikirim');

    }

    private function getAntrian($tanggal){
        $antrian = antrian::where('tanggal', $tanggal)->orderBy('id','desc')->first();

        if (!empty($antrian)) {
            return $antrian->nomor_antrian + 1;
        } else {
            return 1;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('layouts.admin.detail_sttp');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
