<?php

namespace App\Http\Controllers;

use App\Skck;
use App\datadiri;
use App\datakeluarga;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class SkckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.Skck.form_skck');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
    //     $validate = $request->validate([
    //     'nama' => 'required',
    //     'panggilan' => 'required',
    //     'no_ktp' => 'required',
    //     'no_pasport' => 'required',
    //     'agama' => 'required',
    //     'tanggal_lahir' => 'required',
    //     'tempat_lahir' => 'required',
    //     'alamat' => 'required',
    //     'alamat_baru' => 'required',
    //     'kedudukan' => 'required',
    //     'nama_bapak' => 'required|',
    //     'nama_ibu' => 'required',
    //     'alamat_keluarga' => 'required',
    //     'pekerjaan' => 'required',
    //     'suami_istri' => 'required',
    //     'bapak_suami' => 'required',
    //     'ibu_istri' => 'required',
    //     'alamat_suami_istri' => 'required',
    //     'umur_suami_istri' => 'required',
    //     'umur_bapak_suami' => 'required',
    //     'umur_ibu_istri' => 'required',
    //     'saudara_tanggung' => 'required',
    //     'alamat_saudara' => 'required',
    //     'nama_anak' => 'required',
    //     'umur_anak' => 'required',
    //     'ciri_fisik' => 'required',
    //     'rumus_sidik_jari' => 'required',
    //     'riwayat_sekolah' => 'required',
    //     'tahun_lulus' => 'required',
    //     'hobi' => 'required',
    //     'catatan_kriminal' => 'required',
    //     'data_keterangan' => 'required',
    //     'no_hp' => 'required',

    //     'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    //  ]);

        $data = $request->all();
        $datadiri = new datadiri;

        $imageName_foto = 'foto' .time().'.'.$request->foto->extension();
        $request->foto->move(public_path('uploads'), $imageName_foto);

        $datadiri->nama = $data['nama'];
        $datadiri->panggilan = $data['panggilan'];
        $datadiri->no_ktp = $data['no_ktp'];
        $datadiri->no_pasport = $data['no_pasport'];
        $datadiri->agama = $data['agama'];
        $datadiri->jenis_kelamin = $data['jenis_kelamin'];
        $datadiri->tanggal_lahir = $data['tanggal_lahir'];
        $datadiri->tempat_lahir = $data['tempat_lahir'];
        $datadiri->alamat = $data['alamat'];
        $datadiri->alamat_baru = $data['alamat_baru'];
        $datadiri->kedudukan = $data['kedudukan'];
        $datadiri->nama_bapak = $data['nama_bapak'];
        $datadiri->nama_ibu = $data['nama_ibu'];
        $datadiri->alamat_keluarga = $data['alamat_keluarga'];
        $datadiri->pekerjaan= $data['pekerjaan'];
        $datadiri->perlu = $data['perlu'];
        $datadiri->foto = $data['foto'];
        $datadiri->no_hp = $data['no_hp'];
        $datadiri->foto = $imageName_foto;
        $datadiri->save();



        $anak_length = $request->jmlh_anak;

        for ($i = 1; $i <= $anak_length; $i++) {
            // echo $_POST["riwayat_sekolah$i"];
            // echo "<br>";

            DB::table('anak_tabel')->insert(
                [
                    'nama' =>  $_POST["anak$i"],
                    'umur' =>  $_POST["umur_anak$i"],
                    'orangtua_id' => $datadiri->id,
                ]

            );
        }

        $riwayat_length = $request->jmlh_riwayat;

        for ($i = 1; $i <= $riwayat_length; $i++) {
            // echo $_POST["riwayat$i"];
            // echo "<br>";

            DB::table('sekolah_tabel')->insert(
                [
                    'riwayat' =>  $_POST["riwayat$i"],
                    'tahun' =>  $_POST["tahun$i"],
                    'datadiri_id' => $datadiri->id,
                ]

            );
        }


        $datakeluarga = new datakeluarga;
        $datakeluarga->suami_istri = $data['suami_istri'];
        $datakeluarga->bapak_suami = $data['bapak_suami'];
        $datakeluarga->ibu_istri = $data['ibu_istri'];
        $datakeluarga->alamat_suami_istri = $data['alamat_suami_istri'];
        $datakeluarga->umur_suami_istri = $data['umur_suami_istri'];
        $datakeluarga->umur_bapak_suami = $data['umur_bapak_suami'];
        $datakeluarga->umur_ibu_istri = $data['umur_ibu_istri'];
        $datakeluarga->saudara_tanggung = $data['saudara_tanggung'];
        $datakeluarga->alamat_saudara = $data['alamat_saudara'];
        $datakeluarga->save();

        $Skck = new Skck;
        $Skck->ciri_fisik1 = $data['ciri_fisik1'];
        $Skck->ciri_fisik1 = $data['ciri_fisik2'];
        $Skck->ciri_fisik1 = $data['ciri_fisik3'];
        $Skck->ciri_fisik1 = $data['ciri_fisik4'];
        $Skck->ciri_fisik1 = $data['ciri_fisik5'];
        $Skck->hobi = $data['hobi'];
        $Skck->catatan_kriminal1 = $data['catatan_kriminal1'];
        $Skck->catatan_kriminal2 = $data['catatan_kriminal2'];
        $Skck->catatan_kriminal3 = $data['catatan_kriminal3'];
        $Skck->catatan_kriminal4 = $data['catatan_kriminal4'];
        $Skck->data_keterangan1 = $data['data_keterangan1'];
        $Skck->data_keterangan1 = $data['data_keterangan2'];
        $Skck->data_keterangan1 = $data['data_keterangan3'];
        $Skck->data_keterangan1 = $data['data_keterangan4'];
        $Skck->save();
















    //  $imageName_foto = 'foto' .time().'.'.$request->foto->extension();
    //  $request->foto->move(public_path('uploads'), $imageName_foto);

    //  $insertdata = Skck::create([


    //     'foto' => $imageName_foto,


    //  ]);

    //  $insertdata = Skck::create($request->all());

    //  if ($insertdata){
    //      $request->session()->flash('status','<div class="alert alert-sucsess" role="alert">
    //      data telah masuk!
    //    </div>');
    //  }

     return Redirect('index')->with('message', 'Data berhasil di dikirim');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
