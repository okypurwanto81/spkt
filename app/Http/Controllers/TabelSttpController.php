<?php

namespace App\Http\Controllers;

use App\tabelSttp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class TabelSttpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
        {
            $this->middleware('auth');
        }

    public function index()
    {
        $data['sttp_tabel'] = tabelSttp::get();
        return view('layouts.admin.TableSttp', $data);
        // $sttp_tabel = DB::table('sttp_tabel')->paginate(4);
        // return view('layouts.admin.TableSttp',['sttp_tabel' => $sttp_tabel]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sttp_tabel = DB ::table('sttp_tabel')->where('id',$id)->first();
        return view('layouts.admin.detail_sttp', ['sttp_tabel' => $sttp_tabel]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $sttp_tabel = DB ::table('sttp_tabel')->where('id',$id)->first();
      return view('layouts.admin.edit_data',['sttp_tabel' => $sttp_tabel]);
    // echo $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('sttp_tabel')->where('id',$id)->update([
            'nama'          => $request->nama,
            'tempat_tanggal_lahir'   => $request->tempat_tanggal_lahir,
            'agama'          => $request->agama,
            'alamat'          => $request->alamat,
            'nomer_hp'          => $request->nomer_hp,
            'tempat_kegiatan'    => $request->tempat_kegiatan,
            'acara'          => $request->acara,
            'jumlah_peserta'          => $request->jumlah_peserta,
            'kegiatan'      => $request->kegiatan,
            'organisasi'   => $request->organisasi,
            'tebusan'   => $request->tebusan,
            'tanggal_kegiatan' => $request->tanggal_kegiatan,
        ]);

        return Redirect()->route('tabelS')->with('message', 'Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        DB ::table('sttp_tabel')->where('id',$id)->delete();

        return Redirect()->back()->with('message', 'Data berhasil di hapus');

        // echo $id;
    }
}
