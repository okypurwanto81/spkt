<?php

namespace App\Http\Controllers;

use App\datadiri;
use App\tabelizin;
use App\tabelkehilangan;
use App\tabelSttp;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function __construct()
        {
            $this->middleware('auth');
        }

        public function index()
        {
            // $data['datasttp']=count(tabelSttp::where('status', 0)->get());
            // $data['dataizin']=count(tabelizin::where('status', 0)->get());
            // $data['datahilang']=count(tabelkehilangan::where('status', 0)->get());

            $sttpBelumSelesai = tabelSttp::where('status', 0)->count();
            $sttpSelesai = tabelSttp::where('status', 1)->count();


            $izinBelumSelesai = tabelizin::where('status', 0)->count();
            $izinSelesai = tabelizin::where('status', 1)->count();


            $kehilanganBelumSelesai = tabelkehilangan::where('status', 0)->count();
            $kehilanganSelesai = tabelkehilangan::where('status', 1)->count();

            $skckBelumSelesai = datadiri::where('status', 0)->count();
            $skckSelesai = datadiri::where('status', 1)->count();

             return view('layouts.admin.tampilan_admin',
                compact(
                    'sttpBelumSelesai',
                    'sttpSelesai',
                    'izinBelumSelesai',
                    'izinSelesai',
                    'kehilanganBelumSelesai',
                    'kehilanganSelesai',
                    'skckBelumSelesai',
                    'skckSelesai'
            ));
        }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
