<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;




class otentikasiController extends Controller
{
    public function index(){
    return view('otentikasi.login');
}

    public function login(Request $request){

        // dd($request->all());
        // $data = User::where('email', $request->email)->firstOrFail();
        // if ($data){
        //     if(Hash::check($request->password,$data->password)){
        //         return redirect('tampilan_admin');
        //     }
        // }
            if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
                return redirect('tampilan_admin');
            }
            else {
                return redirect('/')->with('message','Email atau Password salah');
            }
    }

    public function logout(Request $request){
        $request->session()->flush();
        return redirect('/');
    }
}
