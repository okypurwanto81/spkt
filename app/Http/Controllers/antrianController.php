<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class antrianController extends Controller
{
    public function kartuAntrian() {

        $gambar = public_path('/assets/img/KARTU ANTRIAN SPKT.jpg');

        $image = imagecreatefromjpeg($gambar);
        $white = imageColorAllocate($image, 255, 255, 255);
        $black = imageColorAllocate($image, 0, 0, 0);

        $fontBold = public_path('/assets/fonts/Quicksand-Bold.ttf');

        $size = 330;

        //definisikan lebar gambar agar posisi teks selalu ditengah berapapun jumlah hurufnya
        $image_width = imagesx($image);

        //membuat textbox agar text centered
        $text_box_QueueNumber = imagettfbbox($size, 0, $fontBold, 2);
        $text_box_QueueDate = imagettfbbox($size, 0, $fontBold, "3232");

        $text_width = $text_box_QueueNumber[2] - $text_box_QueueNumber[0]; // lower right corner - lower left corner
        // $text_height = $text_box_QueueNumber[3] - $text_box_QueueNumber[1];
        $x = ($image_width / 5) - ($text_width / 9);
        $x2 = ($image_width / 6) - ($text_width / 5);
        $x3 = ($image_width / 4.5) - ($text_width / 1);
        $x4 = ($image_width / 3.9) - ($text_width / 1);

        //generate kartu beserta namanya
        imagettftext($image, $size, 0, $x, 600, $white, $fontBold, 23);
        imagettftext($image, 80, 0, $x2, 1170, $black, $fontBold, "Dadadng");
        imagettftext($image, 70, 0, $x3, 1360, $black, $fontBold, 1231);
        imagettftext($image, 70, 0, $x4, 1540, $black, $fontBold, 1231);

        //tampilkan di browser
        header("Content-type:  image/jpeg");
        imagejpeg($image);
        imagedestroy($image);

    }
}
