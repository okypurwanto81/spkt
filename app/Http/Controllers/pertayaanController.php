<?php

namespace App\Http\Controllers;

use App\pertayaan;
use Illuminate\Http\Request;

class pertayaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.Skck.daftarpertanyaan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validateData = $request->validate([
            'nama'          => 'required|max:20|min:3',
            'tanngal'   => 'required|max:100|min:3',
            'agama'          => 'required|max:100|min:3 ',
            'kebangsaan'          => 'required|max:100|min:3',
            'jenis_kelamin'          => 'required|max:100|min:3',
            'status_a'          => 'required|max:100|min:3',
            'kerja'          => 'required|max:100|min:3',
            'alamat'          => 'required|max:100|min:3',
            'ktp'          => 'required|max:100|min:3',
            'no_paspor'          => 'required|max:100|min:3',
            'no_kitas_kitas'          => 'required|max:100|min:3',
            'no_hp'          => 'required|max:100|min:3',


        ],
    // [
    //     'nama.required'  => 'Form harus di isi sesuai KTP',
    //     'tempat_tanggal_lahir.required'  => 'Form harus di isi',
    //     'agama.required'  => 'Form harus di isi',
    //     'alamat.required'  => 'Form harus di isi',
    //     'nomer_hp.required'  => 'Form harus di isi',
    //     'tempat_kegiatan.required'  => 'Form harus di isi',
    //     'acara.required'  => 'Form harus di isi',
    //     'jumlah_peserta.required'  => 'Form harus di isi',
    //     'kegiatan.required'  => 'Form harus di isi',
    //     'organisasi.required'  => 'Form harus di isi',
    //     'tebusan.required'  => 'Form harus di isi',
    //     'tanggal_kegiatan.required'  => 'Form harus di isi',

    // ]

);
    $insertdata = pertayaan::create($request->all());

    if ($insertdata){
        $request->session()->flash('status','<div class="alert alert-sucsess" role="alert">
        data telah masuk!
      </div>');
    }

    return Redirect('index')->with('message', 'Data berhasil di dikirim');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
