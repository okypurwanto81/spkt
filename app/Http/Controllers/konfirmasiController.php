<?php

namespace App\Http\Controllers;

use App\datadiri;
use App\tabelizin;
use App\tabelkehilangan;
use App\tabelSttp;
use Illuminate\Http\Request;
use Exception;
use Twilio\Rest\Client;

class konfirmasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $receiverNumber = "+6282179964818";
        $message = "Selamat bapak mendapatkan hadiah luqman \n \n \n \n \n \n \n \n dari Polisi";
        $sttp = tabelSttp::findOrFail($id);

        try {

            $account_sid = "ACe23bd72968fdf2e4a467761f89be6b79";
            $auth_token = "f19707a3cc532d95c54793e9a5aee408";
            $twilio_number = "+14193180509";

            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_number,
                'body' => $message
            ]);

            $sttp->update([
                'status' => 1
            ]);



            return back();

        } catch (Exception $e) {
            dd("Error: ". $e->getMessage());
        }
    }

    public function izin($id)
    {
        $receiverNumber = "+6282179964818";
        $message = "Selamat bapak mendapatkan hadiah luqman \n \n \n \n \n \n \n \n dari Polisi";
        $izin = tabelizin::findOrFail($id);

        try {

            $account_sid = "ACe23bd72968fdf2e4a467761f89be6b79";
            $auth_token = "f19707a3cc532d95c54793e9a5aee408";
            $twilio_number = "+14193180509";

            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_number,
                'body' => $message
            ]);

            $izin->update([
                'status' => 1
            ]);



            return back();

        } catch (Exception $e) {
            dd("Error: ". $e->getMessage());
        }
    }


    public function hilang($id)
    {
        $receiverNumber = "+6282179964818";
        $message = "Selamat bapak mendapatkan hadiah luqman \n \n \n \n \n \n \n \n dari Polisi";
        $hilang = tabelkehilangan::findOrFail($id);

        try {

            $account_sid = "ACe23bd72968fdf2e4a467761f89be6b79";
            $auth_token = "f19707a3cc532d95c54793e9a5aee408";
            $twilio_number = "+14193180509";

            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_number,
                'body' => $message
            ]);

            $hilang->update([
                'status' => 1
            ]);



            return back();

        } catch (Exception $e) {
            dd("Error: ". $e->getMessage());
        }
    }

    public function skck($id)
    {
        $receiverNumber = "+6282179964818";
        $message = "Selamat bapak mendapatkan hadiah luqman \n \n \n \n \n \n \n \n dari Polisi";
        $skck = datadiri::findOrFail($id);

        try {

            $account_sid = "ACe23bd72968fdf2e4a467761f89be6b79";
            $auth_token = "f19707a3cc532d95c54793e9a5aee408";
            $twilio_number = "+14193180509";

            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_number,
                'body' => $message
            ]);

            $skck->update([
                'status' => 1
            ]);



            return back();

        } catch (Exception $e) {
            dd("Error: ". $e->getMessage());
        }
    }
}
