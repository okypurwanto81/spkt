<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\pertayaan2;

class pertayaan2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.Skck.pertayaan2');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $riwayat_pendidikan_length = $request->jmlh_edu - 1;

        // for ($i = 0; $i < $riwayat_pendidikan_length; $i++) {
        //     // echo $_POST["riwayat_sekolah$i"];
        //     // echo "<br>";

        //     DB::table('anak_tabel')->insert(
        //         [
        //             'nama' =>  $_POST["riwayat_sekolah$i"],
        //             'umur' => 2,
        //             'orangtua_id' => 10,
        //         ]

        //     );
        // }

        $validateData = $request->validate([
            'istri_suami'          => 'required|max:20|min:3',
            'umur'   => 'required|max:100|min:3',
            'agama'          => 'required|max:100|min:3 ',
            'kebangsaan'          => 'required|max:100|min:3',
            'pekerjaan'          => 'required|max:100|min:3',
            'alamat'          => 'required|max:100|min:3',

        ],
    // [
    //     'nama.required'  => 'Form harus di isi sesuai KTP',
    //     'tempat_tanggal_lahir.required'  => 'Form harus di isi',
    //     'agama.required'  => 'Form harus di isi',
    //     'alamat.required'  => 'Form harus di isi',
    //     'nomer_hp.required'  => 'Form harus di isi',
    //     'tempat_kegiatan.required'  => 'Form harus di isi',
    //     'acara.required'  => 'Form harus di isi',
    //     'jumlah_peserta.required'  => 'Form harus di isi',
    //     'kegiatan.required'  => 'Form harus di isi',
    //     'organisasi.required'  => 'Form harus di isi',
    //     'tebusan.required'  => 'Form harus di isi',
    //     'tanggal_kegiatan.required'  => 'Form harus di isi',

    // ]

);
    $insertdata = pertayaan2::create($request->all());

    if ($insertdata){
        $request->session()->flash('status','<div class="alert alert-sucsess" role="alert">
        data telah masuk!
      </div>');
    }

    return Redirect('index')->with('message', 'Data berhasil di dikirim');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
