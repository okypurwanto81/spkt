<?php

namespace App\Http\Controllers;

use App\Suratkehilangan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class tabelkehilanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $kehilangan_tabel = DB::table('kehilangan_tabel')->paginate(5);
        // return view('layouts.admin.tabel_kehilangan',['kehilangan_tabel' => $kehilangan_tabel]);


        $data['kehilangan_tabel'] = Suratkehilangan::get();
        return view('layouts.admin.tabel_kehilangan', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kehilangan_tabel = DB ::table('kehilangan_tabel')->where('id',$id)->first();
        return view('layouts.admin.edit_kehilangan',['kehilangan_tabel' => $kehilangan_tabel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('kehilangan_tabel')->where('id',$id)->update([
            'nama'          => $request->nama,
            'tempat_tanggal_lahir'   => $request->tempat_tanggal_lahir,
            'agama'          => $request->agama,
            'alamat'          => $request->alamat,
            'nomer_hp'          => $request->nomer_hp,
            'tempat_kehilangan'    => $request->tempat_kehilangan,
            'tanggal_kehilanagn'          => $request->tanggal_kehilanagn,
            'keterangan'          => $request->keterangan,
            'waktu_kehilangan' => $request->waktu_kehilangan,
        ]);

        return Redirect()->route('tabel.kehilangan')->with('message', 'Data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        DB ::table('kehilangan_tabel')->where('id',$id)->delete();

        return Redirect()->back()->with('message', 'Data berhasil di hapus');
    }
}
