<?php

namespace App\Http\Controllers;

use App\uploadgambar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
// use Intervention\Image\ImageManager as Image;

class uploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.upload_persyaratan.upload');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([

            'input_fc_kk' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

            'input_fc_ktp' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

            'input_fc_akte' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

            'input_fc_sidik_jari' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $imageName_fc_kk = 'fc_kk' . time().'.'.$request->input_fc_kk->extension();
        // $request->input_fc_kk->move(public_path().'/img', $imageName_fc_kk);

        // read image from temporary file
 $img = Image::make($request->input_fc_kk);

// resize image
  $img->fit(1280, 720);

// save image
 $img->save(public_path().'/img/'.$imageName_fc_kk);

        $imageName_fc_ktp = 'fc_ktp' .time().'.'.$request->input_fc_ktp->extension();
        // $request->input_fc_ktp->move(public_path().'/img', $imageName_fc_ktp);

        $img = Image::make($request->input_fc_ktp);

// resize image
  $img->fit(1280, 720);

// save image
 $img->save(public_path().'/img/'.$imageName_fc_ktp);

        $imageName_fc_akte = 'fc_akte' .time().'.'.$request->input_fc_akte->extension();
        // $request->input_fc_akte->move(public_path().'/img', $imageName_fc_akte);

           // read image from temporary file
 $img = Image::make($request->input_fc_akte);

 // resize image
   $img->fit(1280, 720);

 // save image
  $img->save(public_path().'/img/'.$imageName_fc_akte);

        $imageName_fc_sidik_jari = 'fc_sidik_jari' .time().'.'.$request->input_fc_sidik_jari->extension();
        // $request->input_fc_sidik_jari->move(public_path().'/img', $imageName_fc_sidik_jari);
          // read image from temporary file
 $img = Image::make($request->input_fc_sidik_jari);

 // resize image
   $img->fit(1280, 720);

 // save image
  $img->save(public_path().'/img/'.$imageName_fc_sidik_jari);



        $insertdata = uploadgambar::create([


            'input_fc_kk' => $imageName_fc_kk,
            'input_fc_ktp' => $imageName_fc_ktp,
            'input_fc_akte' => $imageName_fc_akte,
            'input_fc_sidik_jari' => $imageName_fc_sidik_jari,


        ]);

        if ($insertdata){
            $request->session()->flash('status','data telah masuk');
        }

        return Redirect('index')->with('message', 'Data berhasil di simpan');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
