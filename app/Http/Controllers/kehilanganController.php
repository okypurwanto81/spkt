<?php

namespace App\Http\Controllers;

use App\Suratkehilangan;
use Illuminate\Http\Request;

class kehilanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.Surat_kehilangan.form_kehilangan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'nama'          => 'required|max:20|min:3',
            'tempat_tanggal_lahir'   => 'required|max:100|min:3',
            'agama'          => 'required|max:100|min:3 ',
            'alamat'          =>'required|max:100|min:3',
            'nomer_hp'          => 'required|max:100|min:1',
            'waktu_kehilangan'    => 'required|max:100|min:3',
            'tempat_kehilangan'          => 'required|max:100|min:3',
            'tanggal_kehilanagn'          => 'required|max:100|min:3',
            'keterangan'            => 'required|max:500|min:3',
        ],

    [
        'nama.required'  => 'Form harus di isi sesuai KTP',
        'tempat_tanggal_lahir.required'  => 'Form harus di isi',
        'agama.required'  => 'Form harus di isi',
        'alamat.required'  => 'Form harus di isi',
        'nomer_hp.required'  => 'Form harus di isi',
        'waktu_kehilangan.required' => 'Form harus di isi',
        'tempat_kehilangan.required' =>  'Form harus di isi',
        'tanggal_kehilanagn.required' =>  'Form harus di isi',
        'keterangan.required'  =>  'Form harus di isi',
    ]

);
    $insertdata = Suratkehilangan::create($request->all());

    if ($insertdata){
        $request->session()->flash('status','<div class="alert alert-sucsess" role="alert">
        data telah masuk!
      </div>');
    }

    return redirect('index')->with('message', 'Data berhasil di dikirim');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
