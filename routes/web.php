<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::get('/','otentikasiController@index')->name('login');
route::post('login','otentikasiController@login')->name('login');

// route::get('registers', 'Auth\RegisterController@index')->name('register');

//dasbord
Route::get('/index', 'indexController@index')->name('dasboard');

//sttp
Route::get('/from_sttp', 'Sttp2Controller@index')->name('sttp');
Route::post('/simpan-data', 'Sttp2Controller@store')->name('simpan-data');
// Route::get('detail-data/{id}', 'Sttp2Controller@show')->name('detai-data');

//izin
Route::get('form_izin', 'izinController@index')->name('izin');
Route::post('/simpan-izin', 'izinController@store')->name('simpan-izin');

//kehilangan
Route::get('form_kehilangan', 'kehilanganController@index')->name('kehilangan');
route::post('simpan-kehilangan', 'kehilanganController@store')->name('simpan-kehilangan');

//skck
Route::get('form_skck', 'SkckController@index')->name('skck');
Route::post('simpan-skck', 'SkckController@store')->name('simpan-skck');
Route::get('daftarpertanyaan', 'pertayaanController@index')->name('pertayaan');
Route::get('pertayaan2', 'pertayaan2Controller@index')->name('pertayaan2');
Route::post('pertayaan2', 'pertayaan2Controller@store')->name('pertayaan2.post');
Route::get('pertayaan3', 'pertayaan3Controller@index')->name('pertayaan3');
//upload
Route::get('upload', 'uploadController@index')->name('upload');
Route::post('simpan-gambar', 'uploadController@store')->name('simpan-gambar');

Route::get('/lihat-antrian', 'antrianController@kartuAntrian');



//Admin
Route::group(['middleware' => ['auth']], function () {
    //tampilanAdmin
    Route::get('tampilan_admin', 'AdminController@index')->name('admin');
    //sttp tabel
    Route::get('TableSttp', 'TabelSttpController@index')->name('tabelS');
    Route::get('TableSttp/{id}', 'TabelSttpController@edit')->name('tabel.edit');
    Route::delete('delete/TableSttp/{id}', 'TabelSttpController@delete')->name('tabel.delete');
    Route::patch('TableSttp/{id}', 'TabelSttpController@update')->name('tabel.update');
    Route::get('detail-data/{id}', 'TabelSttpController@show')->name('detai-data');
    //izintabel
    Route::get('tabel_izin', 'TabelizinpController@index')->name('tabel.izin');
    Route::get('tabel_izin/{id}', 'TabelizinpController@edit')->name('edit.izin');
    Route::delete('delete/tabel_izin/{id}', 'TabelizinpController@delete')->name('delete.izin');
    Route::patch('tabel_izin/{id}', 'TabelizinpController@update')->name('izin.update');
    //kehilagan table
    Route::get('tabel_kehilangan', 'tabelkehilanganController@index')->name('tabel.kehilangan');
    Route::get('tabel_kehilangan/{id}', 'tabelkehilanganController@edit')->name('edit.kehilangan');
    Route::delete('delete/tabel_kehilangan/{id}', 'tabelkehilanganController@delete')->name('delete.kehilangan');
    Route::patch('tabel_kehilangan/{id}', 'tabelkehilanganController@update')->name('kehilangan.update');
    //
    route::get('logout', 'otentikasiController@logout')->name('logout');
    Route::get('tabel_gambar', 'tabel_gambarController@index')->name('tabel-gambar');
    Route::get('tabelskck', 'tabel_skckController@index')->name('skck-tabel');
    Route::delete('delete/tabelskck/{id}', 'tabel_skckController@delete')->name('delete.skck');
    //
    route::get('grafik', 'grafikController@index')->name('grafik');
    //
    Route::get('konfirmasi/{id}', 'konfirmasiController@index')->name('konfir');
    Route::get('konfirmasiizin/{id}', 'konfirmasiController@izin')->name('konfirizin');
    Route::get('konfirmasihilang/{id}', 'konfirmasiController@hilang')->name('hilang');
    Route::get('konfirmasiskck/{id}', 'konfirmasiController@skck')->name('konfirskck');
});
//uplod

// Route::post('simpan-gambar', 'uploadController@store')->name('simpan-gambar');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
